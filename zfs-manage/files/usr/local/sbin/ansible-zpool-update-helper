#!/usr/bin/python3
import yaml
import yaml.loader
import sys
import subprocess
import re
from os.path import basename
import getopt

proc_name = basename(sys.argv[0])

VDEV_RE = r'^(((spare|log|dedup|special|cache) ){0,1}(mirror|raidz1|raidz2|raidz3|raidz|disk|file))($|-.*$)'


def read_ansible_yaml():
    """Read domain config from stdin as yaml"""
    try:
        request = yaml.load(sys.stdin, Loader=yaml.loader.SafeLoader)
    except Exception as err:
        print('{0}: Invalid yaml on stdin - {1}'.format(proc_name, err), file=sys.stderr)
        sys.exit(2)

    if request == None:
        print('{0}: No Input - program to be called from Ansible'.format(proc_name), file=sys.stderr)
        sys.exit(2)

    return request

def zpool_status(pool):
    """Get Zpool status"""
    # Process info for pool being expanded or shrunk
    try:
        list_out = subprocess.check_output(['zpool', 'status', '-p', pool])
    except subprocess.SubprocessError as err:
        print("{0}: Error calling 'zpool status' - '{1}'".format(proc_name, err), file=sys.stderr)
        sys.exit(2)

    list_out = list_out.decode().split('\n')
    config_flag = False
    list_out_new = []
    for x in list_out:
        if re.match(r'^\s{0,4}config:.*$',x):
            config_flag = True
            continue
        if not config_flag:
            continue
        list_out_new.append(x)
    list_out = list_out_new
    list_out = [ x for x in list_out if not re.match(r'^\s{0,4}(NAME|errors:).*$',x)]
    list_out = [ x.strip('\t') for x in list_out ]
    list_out = [ x for x in list_out if x != '']
    list_out = [ re.sub(r'^(\s*\S+)\s+.*$', r'\1', x) for x in list_out ]
    return list_out

def check_request(request):
    if type(request) is not list:
        print("{0}: request must be a list."
              .format(proc_name),
              file=sys.stderr)
        sys.exit(2)
    error = False
    for vdev in request:
        if (type(vdev) is list):
            if (len(vdev) == 0):
                print("{0}: vdev cannot be an empty list."
                      .format(proc_name),
                      file=sys.stderr)
                error = True
                continue
            elif (len(vdev) > 1):
                if (not re.match(VDEV_RE, vdev[0])):
                    print("{0}: In vdev '{1}' first element must match '{2}'"
                          .format(proc_name, vdev, VDEV_RE),
                          file=sys.stderr)
                    error=True
                    continue
        elif (type(vdev) is not str):
            print("{0}: vdev '{1}' must be a list or a string."
                  .format(proc_name, vdev),
                  file=sys.stderr)
            error = True
            continue
    if error:
        sys.exit(2)
    return

def preprocess_request_dev(dev):
    if (type(dev) is str and dev.find('/dev/') == 0):
        dev_split = dev.split('/')
        if (len(dev_split) == 3):
            return dev_split[-1]
        else:
            return dev
    else:
        return dev

def preprocess_request(request):
    new_request = []
    for vdev in request:
        if (type(vdev) is list):
            new_vdev = []
            for item in vdev:
                new_item = preprocess_request_dev(item)
                new_vdev.append(new_item)
            new_request.append(new_vdev)
        elif (type(vdev) is str):
                new_item = preprocess_request_dev(vdev)
                new_request.append([new_item])
        else:
            new_request.append(vdev)
    return new_request

def vdev_name(x):
    if len(x) == 1:
        return [x[0]]
    if (re.match(VDEV_RE, x[0])):
        actual = x[0].split(' ')[-1]
        return [actual]
    return x[1:]

def flatten(l):
    f = []
    for t in l:
        if type(t) is list:
            for x in t:
                f.append(x)
        else:
            f.append(t)
    return f

def set_to_vdev(s):
    first = None
    for x in s:
        if re.match(VDEV_RE, x):
            if first != None:
                print("{0}: Error- incongruent vdev definition - '{1}' - blowing up!"
                      .format(proc_name, s),
                      file=sys.stderr)
                sys.exit(2)
            first = x
    vdev = [x for x in s if x != first]
    if first:
        vdev = [first,] + vdev
    return vdev

def short_usage():
    print("Usage: '{0} [-dfhn] <zfs-pool-name>'".format(proc_name), file=sys.stderr)

def full_usage():
    short_usage()
    print('', file=sys.stderr)
    print("\tFor shrinking or expanding a ZFS pool from Anathoth Ansible", file=sys.stderr)
    print('', file=sys.stderr)
    print("\t-d --debug      Print out debugging", file=sys.stderr)
    print("\t-f --force-add  Force addition of vdev", file=sys.stderr)
    print("\t-h --help       Print this help message", file=sys.stderr)
    print("\t-n --dry-run    Dry run", file=sys.stderr)

try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], 'dfhn', ['debug',  'force-add', 'help', 'dry-run'])
except getopt.GetoptError:
    short_usage()
    sys.exit(2)

dry_run = False
debug = False
force_add = False
for opt in opts:
    if opt[0] in ('-d', '--debug'):
        debug = True
    if opt[0] in ('-f', '--force-add'):
        force_add = True
    if opt[0] in ('-n', '--dry-run'):
        dry_run = True
    if opt[0] in ('-h', '--help'):
        full_usage()
        sys.exit(0)

if len(args) < 1:
    print('{0}: Not enough args - please supply ZFS pool name'.format(proc_name), file=sys.stderr)
    sys.exit(2)
pool = args[0]

request = read_ansible_yaml()
check_request(request)
list_out = zpool_status(pool)
list_makeup = []
vdev = []
sp_pool = ''
for item in list_out:
    if item == pool:
        continue
    if (item == 'logs'):
        sp_pool = 'log'
        continue
    elif (item in ('cache', 'special', 'dedeup')):
        sp_pool = item
        continue
    item_match = re.match(r'^((  )|(    ))(\S+)$', item)
    if not item_match:
        # Blow up
        print("{0}: Error - unexpected output from 'zpool status -p {1}' - '{2}'"
              .format(proc_name, pool, item),
              file=sys.stderr)
        sys.exit(255)
    if item_match.group(1) == '  ':
        if vdev:
            list_makeup.append(vdev)
        vdev = [ ((sp_pool + ' ') if sp_pool else '') + item_match.groups()[-1], ]
        sp_pool = ''
        continue
    elif item_match.group(1) == '    ':
        vdev.append(item_match.groups()[-1])
        continue
    else:
        # Blow up
        print("{0}: Error - unexpected output from 'zpool status -p {1}' - '{2}'"
              .format(proc_name, pool, item),
              file=sys.stderr)
        sys.exit(255)
if vdev:
    list_makeup.append(vdev)
    vdev = []
    sp_pool = ''

list_makeup_type = [([re.sub(VDEV_RE, r'\1', x[0])] + x[1:]) for x in list_makeup ]
list_makeup_type_set = [set(x) for x in list_makeup_type]


list_makeup_name = [vdev_name(x) for x in list_makeup]
list_makeup_name_zip = zip(list_makeup_type_set, list_makeup_name)

diff = lambda l1,l2: [x for x in l1 if x not in l2]

if debug:
    print('list_out - {0}'.format(list_out), file=sys.stderr)
    print('list_makeup - {0}'.format(list_makeup), file=sys.stderr)
    print('list_makeup_name - {0}'.format(list_makeup_name), file=sys.stderr)
    print('list_makeup_type - {0}'.format(list_makeup_type), file=sys.stderr)
    print('list_makeup_type_set - {0}'.format(list_makeup_type_set), file=sys.stderr)
    print('request - {0}'.format(request), file=sys.stderr)

request = preprocess_request(request)
request_set = [set(x) for x in request]

diff_expand = diff(request_set, list_makeup_type_set)
if diff_expand:
    if debug:
        print('Expanding array', file=sys.stderr)
    extra_args = ['-f',] if force_add else []
    diff_expand_list = [set_to_vdev(x) for x in diff_expand]
    zpool_add_args = ['zpool', 'add',] + extra_args + [pool,] + flatten(diff_expand_list)
    zpool_add_args = flatten([x.split(' ') for x in zpool_add_args])
    if debug:
        print('zpool_add_args - {0}'.format(zpool_add_args), file=sys.stderr)
    if not dry_run:
        try:
            ret_code_add = subprocess.check_call(zpool_add_args)
        except subprocess.SubprocessError as err:
            print("{0}: Error calling 'zpool add' - '{1}'".format(proc_name, err), file=sys.stderr, flush=True)
            sys.exit(2)
    else:
        ret_code_add = 0
    if debug:
        print('ret_code_add - {0}'.format(ret_code_add), file=sys.stderr)
    if (ret_code_add == 0):
        print('Changed')
    sys.exit(ret_code_add)

diff_shrink = diff(list_makeup_type_set, request_set)
if diff_shrink:
    if debug:
        print('Shrinking array', file=sys.stderr)
    # Work out args for remove
    remove_names = flatten([ x[1] for x in list_makeup_name_zip if x[0] in diff_shrink])
    zpool_remove_args = ['zpool', 'remove', pool] + remove_names 
    if debug:
        print('zpool_remove_args - {0}'.format(zpool_remove_args), file=sys.stderr)
    if not dry_run:
        try:
            ret_code_remove = subprocess.check_call(zpool_remove_args)
        except subprocess.SubprocessError as err:
            print("{0}: Error calling 'zpool remove' - '{1}'".format(proc_name, err), file=sys.stderr)
            sys.exit(2)
    else:
        ret_code_remove = 0
    if debug:
        print('ret_code_remove - {0}'.format(ret_code_remove), file=sys.stderr)
    if (ret_code_remove == 0):
        print('Changed')
    sys.exit(ret_code_remove)

print('No change')
sys.exit(0)
