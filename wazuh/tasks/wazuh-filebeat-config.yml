---
# Wazuh filebeat config
#
- block:
  - name: Wazuh - set wazuh_force_filebeat_module_update=true to force module update/refresh
    debug:
      msg: "{{ wazuh_force_user_update|d(false) }}"

  - name: Wazuh - configure filebeat.yml
    template:
      src: etc/filebeat/filebeat.yml
      dest: /etc/filebeat/filebeat.yml
      owner: root
      group: root
      mode: '0400'
    no_log: "{{ not(display_no_log|default(False)) }}"
    notify:
      - wazuh filebeat restart

  - name: Wazuh - fetch latest Wazuh filebeat alerts template locally
    become: yes
    become_user: admin
    local_action:
      module: get_url
      url: https://raw.githubusercontent.com/wazuh/wazuh/{{ wazuh_template_branch }}/extensions/elasticsearch/7.x/wazuh-template.json
      dest: "files/wazuh/filebeat-wazuh-template.json"
    failed_when: false

  - name: Wazuh - copy filebeat alerts template to server
    copy:
      src: "files/wazuh/filebeat-wazuh-template.json"
      dest: "/etc/filebeat/wazuh-template.json"
      owner: root
      group: root
      mode: '0440'
    notify:
      - wazuh filebeat restart

  - name: Wazuh - stat for filebeat keystore
    stat:
      path: /var/lib/filebeat/filebeat.keystore
    register: wazuh_result_filebeat_keystore_stat

  - name: Wazuh - create filebeat keystore if not there
    command: filebeat keystore create
    when: not wazuh_result_filebeat_keystore_stat.stat.exists

  - block:
    - name: Wazuh - update filebeat default user
      command:
        argv:
          - filebeat
          - keystore
          - add
          - username
          - '--stdin'
          - '--force'
        stdin: |
          admin
      changed_when: false

    - name: Wazuh - update filebeat default password
      expect:
        echo: yes
        command: "filebeat keystore add password --force"
        responses:
          'password:': "{{ wazuh_indexer_admin_password }}"
      no_log: "{{ not(display_no_log|default(False)) }}"
      changed_when: false

    when: wazuh_result_rest_install.changed|d(false) or wazuh_force_user_update|d(false)
    tags:
      - always

  - name: Wazuh - remove filebeat module dir
    file:
      path: /usr/share/filebeat/module/wazuh
      state: absent
    when: wazuh_force_filebeat_module_update|d(false)

  - name: Wazuh - create filebeat module extract dir
    file:
      path: /usr/share/filebeat/module/wazuh
      state: directory
      owner: root
      group: root
      mode: '0755'

  - name: Wazuh - extract Wazuh filebeat module
    unarchive:
      src: "{{ wazuh_filebeat_module_tgz }}"
      dest: "{{ wazuh_filebeat_module_dir }}"
      extra_opts:
        - --strip
        - '1'
      # Unpacking tarball again replaces files?
      creates: "{{ wazuh_filebeat_module_dir }}/module.yml"
      owner: root
      group: root
    notify:
      - wazuh filebeat restart

  - name: Wazuh - create filebeat certs dir
    file:
      state: directory
      path: /etc/filebeat/certs
      owner: root
      group: root
      mode: '0500'

  - name: Wazuh - copy filebeat key file
    copy:
      src: wazuh/certificates/wazuh-certificates/wazuh-indexer-key.pem
      dest: /etc/filebeat/certs/filebeat-key.pem
      owner: root
      group: root
      mode: '0400'
    notify:
      - wazuh filebeat restart

  - name: Wazuh - copy filebeat certs file
    copy:
      src: wazuh/certificates/wazuh-certificates/wazuh-indexer.pem
      dest: /etc/filebeat/certs/filebeat.pem
      owner: root
      group: root
      mode: '0400'
    notify:
      - wazuh filebeat restart

  - name: Wazuh - copy wazuh-filebeat certs and keys
    copy:
      src: 'wazuh/certificates/wazuh-certificates/{{ item }}'
      dest: '/etc/filebeat/certs/{{ item }}'
      owner: root
      group: root
      mode: '0400'
    loop:
      - root-ca.pem
    notify:
      - wazuh filebeat restart

  - block:
    - name: Wazuh - reload systemd
      systemd:
        daemon_reload: true

    - name: Wazuh - enable and start filebeat
      systemd:
        name: filebeat.service
        enabled: yes
        state: started

    when: wazuh_result_rest_install.changed|d(false)
    tags:
      - always

  - name: Wazuh - test filebeat
    shell: filebeat test output 2>&1
    register: wazuh_result_filebeat_test
    changed_when: false

  - name: Wazuh - display filebeat output if error
    debug:
      msg: |
        {{ wazuh_result_filebeat_test.stdout }}
    when: wazuh_result_filebeat_test.rc != 0

  - name: Wazuh - setup filebeat pipelines
    command: filebeat setup --pipelines
    changed_when: false

  - name: Wazuh - setup index management
    command: filebeat setup --index-management -E output.logstash.enabled=false
    changed_when: false

  tags:
    - always

...
