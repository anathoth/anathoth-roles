# {{ ansible_managed }}
#
# Ansible generated network file
[Match]
Name={{ if.ifname }}

{% set config = if.systemd.config|d({}) if (if.systemd.config is defined and not (if.systemd.config.keys()|difference(systemd_config_keys))) else if.systemd.SYNTAX_config_keys %}
{% set network = if.systemd.network|d() %}
[Link]
{% if systemd_version is version('241', '>=') %}
{% if config.unmanaged is defined %}
{% set unmanaged = config.unmanaged|d(false) if config.unmanaged|d(false) in [true,false] else config.SYNTAX_unmanaged %}
Unmanaged={{ unmanaged|ternary('yes','no') }}
{% endif %}
{% endif %}
{% if config.mtu is defined %}
{%  set mtu_bytes = config.mtu if config.mtu|regex_search('^[0-9]{3,4}$') else config.SYNTAX_mtu %}
MTUBytes={{ mtu_bytes }}
{% endif %}
{% for section in network if section.section|d() == 'Link' %}
{{ section.settings }}
{% endfor %}

[Network]
{% set nameservers = config.namservers|d() if config.nameservers is defined and not config.nameservers.keys()|difference(systemd_nameservers_keys) else config.SYNTAX_nameservers_keys %}
{% set dhcp4 = config.dhcp4|d(false) if config.dhcp4|d(false) in [true,false] else config.SYNTAX_dhcp4 %}
{% set dhcp6 = config.dhcp6|d(false) if config.dhcp6|d(false) in [true,false] else config.SYNTAX_dhcp6 %}
{% if dhcp4 and not dhcp6 %}
DHCP=ipv4
{% elif not dhcp4 and dhcp6 %}
DHCP=ipv6
{% elif dhcp4 and dhcp6 %}
DHCP=yes
{% endif %}
{% if config['use_domains'] is defined %}
{% set use_domains = config['use_domains']|d(false) if config['use_domains']|d(false) in [true,false,'route'] else config.SYNTAX_use_domains %}
UseDomains={{ use_domains|ternary('yes','no') if use_domains in [true,false] else use_domains }}
{% endif %}
{% for addr in config.addresses|d([]) %}
{% set addr = addr if addr|ipaddr else addr.SYNTAX_addresses %}
Address={{ addr }}
{% endfor %}
{% if config.gateway4|d() %}
{% set gateway = config.gateway4 if config.gateway4|ipaddr else addr.SYNTAX_gateway4 %}
Gateway={{ gateway }}
{% endif %}
{% if config.gateway6|d() %}
{% set gateway = config.gateway6 if config.gateway6|ipaddr else addr.SYNTAX_gateway6 %}
Gateway={{ gateway }}
{% endif %}
{% if config.keep_configuration is defined %}
{%   set keep_configuration = config.keep_configuration|d(false) %}
{%   set keep_configuration = keep_configuration if keep_configuration in [true,false,'dhcp','static','dhcp-on-stop'] else config.SYNTAX_keep_configuration %}
{%   if keep_configuration in [true,false] %}
KeepConfiguration = {{ keep_configuration|ternary('yes', 'no') }}
{%   else %}
KeepConfiguration = {{ keep_configuration }}
{%   endif %}
{% endif %}
{% if config['accept-ra'] is defined or config.accept_ra is defined %}
{% if config['accept-ra'] is defined and config.accept_ra is defined %}
{% set accept_ra = config.SYNTAX_accept_ra_multiple %}
{% endif %}
{% set accept_ra = config.accept_ra|d(false) if config.accept_ra is defined else config['accept-ra']|d(false) %}
{% set accept_ra = accept_ra if accept_ra in [true,false] else config.SYNTAX_accept_ra %}
IPv6AcceptRA={{ accept_ra|ternary('yes','no') }}
{% endif %}
{% if nameservers|d() %}
{% for addr in nameservers.addresses|d([]) %}
{% set addr = addr if addr|ipaddr else addr.SYNTAX_nameservers_addresses %}
DNS={{ addr }}
{% endfor %}
{% for domain in nameservers.search|d([]) %}
{% set domain = domain if domain|regex_search('^~?[-.a-zA-Z0-9_]+$') else config.SYNTAX_nameservers_search %}
{% endfor %}
{% if nameservers.search|d([]) %}
Domains={{ nameservers.search|join(' ') }}
{% endif %}
{% endif %}
{% if config.llmnr is defined %}
{% set llmnr = config.llmnr|d(true) if config.llmnr|d(true) in [true,false,'resolve'] else config.SYNTAX_llmnr %}
LLMNR={{ llmnr|ternary('yes','no') if llmnr in [true,false] else llmnr }}
{% endif %}
{% if config.mdns is defined %}
{% set mdns = config.mdns|d(false) if config.mdns|d(false) in [true,false,'resolve'] else config.SYNTAX_mdns %}
MulticastDNS={{ mdns|ternary('yes','no') if mdns in [true,false] else mdns }}
{% endif %}
{% if config.dnssec is defined %}
{% set dnssec = config.dnssec|d(false) if config.dnssec|d(false) in [true,false,'allow-downgrade'] else config.SYNTAX_dnssec %}
DNSSEC={{ dnssec|ternary('yes','no') if dnssec in [true,false] else dnssec }}
{% endif %}
{% for domain in config['dnssec_negative_trust_anchors']|d([]) %}
{% set domain = domain if domain|regex_search('^[-.a-zA-Z0-9_]+$') else config.SYNTAX_dnssec_negative_trust_anchors %}
{% endfor %}
{% if config['dnssec_negative_trust_anchors']|d([]) %}
DNSSSECNegativeTrustAnchors={{ (config['dnssec_negative_trust_anchors'])|join(' ') }}
{% endif %}
{% if config['ipv6_send_ra'] is defined %}
{% set ipv6_send_ra = config['ipv6_send_ra']|d(false) if config['ipv6_send_ra']|d(false) in [true,false] else config.SYNTAX_ipv6_send_ra %}
IPv6SendRA={{ ipv6_send_ra|ternary('yes','no') }}
{% endif %}
{% if config['dhcp_server'] is defined %}
{% set dhcp_server = config['dhcp_server']|d(false) if config['dhcp_server']|d(false) in [true,false] else config.SYNTAX_dhcp_server %}
DHCPServer={{ dhcp_server|ternary('yes','no') }}
{% endif %}
{% for section in network if section.section|d() == 'Network' %}
{{ section.settings }}
{% endfor %}

{% if config.ipv6_send_ra|d(false) %}
{% set ipv6_ra = if.systemd.config.ipv6_ra|d({}) if (if.systemd.config is defined  and if.systemd.config.ipv6_ra is defined and not (if.systemd.config.ipv6_ra.keys()|difference(systemd_ipv6_ra_keys))) else config.SYNTAX_ipv6_ra_keys %}
[IPv6SendRA]
{% if ipv6_ra.managed is defined %}
{% set managed = ipv6_ra.managed|d(false) if ipv6_ra.managed|d(false) in [true,false] else ipv6_ra.SYNTAX_managed %}
Managed={{ managed|ternary('yes','no') }}
{% endif %}
{% if ipv6_ra.other_information is defined %}
{% set other_information = ipv6_ra.other_information|d(false) if ipv6_ra.other_information|d(false) in [true,false] else ipv6_ra.SYNTAX_other_information %}
OtherInformation={{ other_information|ternary('yes','no') }}
{% endif %}
{% if ipv6_ra.router_lifetime_sec is defined %}
{% set router_lifetime_sec = ipv6_ra.router_lifetime_sec if (ipv6_ra.router_lifetime_sec|regex_search('^[0-9]+$') and ipv6_ra.router_lifetime_sec >= 0)  else ipv6_ra.SYNTAX_router_lifetime_sec %}
RouterLifetimeSec={{ router_lifetime_sec }}
{% endif %}
{% if ipv6_ra.router_preference is defined %}
{% set router_preference = ipv6_ra.router_preference if ipv6_ra.router_preference in ['high', 'medium', 'low'] else ipv6_ra.SYNTAX_router_preference %}
RouterPreference={{ router_preference }}
{% endif %}
{% if ipv6_ra.emit_dns is defined %}
{% set emit_dns = ipv6_ra.emit_dns|d(false) if ipv6_ra.emit_dns|d(false) in [true,false] else ipv6_ra.SYNTAX_emit_dns %}
EmitDNS={{ emit_dns|ternary('yes','no') }}
{% endif %}
{% if ipv6_ra.emit_domains is defined %}
{% set emit_domains = ipv6_ra.emit_domains|d(false) if ipv6_ra.emit_domains|d(false) in [true,false] else ipv6_ra.SYNTAX_emit_domains %}
EmitDomains={{ emit_domains|ternary('yes','no') }}
{% endif %}
{% if ipv6_ra.dns_lifetime_sec is defined %}
{% set dns_lifetime_sec = ipv6_ra.dns_lifetime_sec if (ipv6_ra.dns_lifetime_sec|regex_search('^[0-9]+$') and ipv6_ra.dns_lifetime_sec >= 0)  else ipv6_ra.SYNTAX_dns_lifetime_sec %}
DNSLifetimeSec={{ dns_lifetime_sec }}
{% endif %}
{% for addr in ipv6_ra.dns|d([]) %}
{% set addr = addr if addr|ipaddr else ipv6_ra.SYNTAX_nameservers_addresses %}
DNS={{ addr }}
{% endfor %}
{% for domain in ipv6_ra.domains|d([]) %}
{% set domain = domain if domain|regex_search('^~?[-.a-zA-Z0-9_]+$') else ipv6_ra.SYNTAX_nameservers_domains %}
{% endfor %}
{% if ipv6_ra.domains|d([]) %}
Domains={{ ipv6_ra.domains|join(' ') }}
{% endif %}
{% for section in network if section.section|d() == 'IPv6SendRA' %}
{{ section.settings }}
{% endfor %}
{% endif %}

{% set ipv6_ra_prefixes = if.systemd.config.ipv6_ra_prefixes|d([]) if (if.systemd.config is defined  and if.systemd.config.ipv6_ra_prefixes is defined) %}
{% for prefix_raw in ipv6_ra_prefixes|d([]) %}
{% set prefix = prefix_raw if not (prefix_raw.keys()|difference(systemd_ipv6_ra_prefixes_keys)) else ipv6_ra_prefixes.SYNTAX_ipv6_ra_prefixes_keys %}
[IPv6Prefix]
{% if prefix.prefix is defined %}
{% set prefix_1 = prefix.prefix if prefix.prefix|ipv6 else ipv6_ra_prefixes.SYNTAX_prefix %}
Prefix={{ prefix_1 }}
{% endif %}
{% if prefix.address_autoconfiguration is defined %}
{% set address_autoconfiguration = prefix.address_autoconfiguration|d(false) if prefix.address_autoconfiguration|d(false) in [true,false] else ipv6_ra_prefixes.SYNTAX_address_autoconfiguration %}
AddressAutoconfiguration={{ address_autoconfiguration|ternary('yes','no') }}
{% endif %}
{% if prefix.on_link is defined %}
{% set on_link = prefix.on_link|d(false) if prefix.on_link|d(false) in [true,false] else ipv6_ra_prefixes.SYNTAX_on_link %}
OnLink={{ on_link|ternary('yes','no') }}
{% endif %}
{% if prefix.preferred_lifetime_sec is defined %}
{% set preferred_lifetime_sec = prefix.preferred_lifetime_sec if (prefix.preferred_lifetime_sec|regex_search('^[0-9]+$') and prefix.preferred_lifetime_sec >= 0)  else ipv6_ra_prefixes.SYNTAX_preferred_lifetime_sec %}
PreferredLifetimeSec={{ preferred_lifetime_sec }}
{% endif %}
{% if prefix.valid_lifetime_sec is defined %}
{% set valid_lifetime_sec = prefix.valid_lifetime_sec if (prefix.valid_lifetime_sec|regex_search('^[0-9]+$') and prefix.valid_lifetime_sec >= 0)  else ipv6_ra_prefixes.SYNTAX_valid_lifetime_sec %}
ValidLifetimeSec={{ valid_lifetime_sec }}
{% endif %}
{% if prefix.assign is defined %}
{% set assign = prefix.assign|d(false) if prefix.assign|d(false) in [true,false] else ipv6_ra_prefixes.SYNTAX_assign %}
Assign={{ assign|ternary('yes','no') }}
{% endif %}

{% endfor %}
{% set ipv6_ra_route_prefixes = if.systemd.config.ipv6_ra_route_prefixes|d([]) if (if.systemd.config is defined  and if.systemd.config.ipv6_ra_route_prefixes is defined) %}
{% for route_raw in ipv6_ra_route_prefixes|d([]) %}
{% set route = route_raw if not (route_raw.keys()|difference(systemd_ipv6_ra_route_prefixes_keys)) else ipv6_ra_route_prefixes.SYNTAX_ipv6_ra_route_prefixes_keys %} 
[IPv6RoutePrefix]
{% if route.route is defined %}
{% set route_1 = route.route if route.route|ipv6 else ipv6_ra_route_prefixes.SYNTAX_route %}
Route={{ route_1 }}
{% endif %}
{% if route.lifetime_sec is defined %}
{% set lifetime_sec = route.lifetime_sec if (route.lifetime_sec|regex_search('^[0-9]+$') and route.lifetime_sec >= 0)  else ipv6_ra_route_prefixes.SYNTAX_lifetime_sec %}
LifetimeSec={{ lifetime_sec }}
{% endif %}

{% endfor %}

{% for section in network if section.section|d() not in ['Network', 'Link', 'Match', 'IPv6SendRA'] %}
{% if section.section|d() %}
[{{section.section }}]
{% endif %}
{% if section.settings|d() %}
{{ section.settings }}
{% endif %}

{% endfor %}
