---
# Configure strongswan
- block:
  - name: Config strongswan charon.conf - set values
    lineinfile:
      path: /etc/strongswan.d/charon.conf
      regexp: "[ #]*{{ item.key }} =.*$"
      line: "    {{ item.key }} = {{ item.value|ternary('yes','no') if (item.value is sameas false or item.value is sameas true) else item.value }}"
      owner: root
      group: root
      mode: 0644
    when: item.value is not none
    loop: "{{ strongswan_var_charon_settings|dict2items }}"
    notify:
      - restart strongswan

  - name: Config strongswan charon.conf - unset values
    lineinfile:
      path: /etc/strongswan.d/charon.conf
      regexp: "[ #]*{{ item.key }} =.*$"
      line: "    # {{ item.key }} = "
      owner: root
      group: root
      mode: 0644
    when: item.value is none
    loop: "{{ strongswan_var_charon_settings|dict2items }}"
    notify:
      - restart strongswan

  - include_tasks: config-strongswan-charon-plugin.yml
    loop: "{{ strongswan_var_charon_plugin_settings|dict2items }}"
    loop_control:
      loop_var: outer_item

  - name: Template apparmor config for Certbot
    template:
      src: etc/apparmor.d/local/usr.lib.ipsec.charon
      dest: /etc/apparmor.d/local/usr.lib.ipsec.charon
      owner: root
      group: root
      mode: '0644'
    register: strongswan_result_apparmor_updated

  - name: Validate apparmor config change
    command: apparmor_parser -d /etc/apparmor.d/usr.lib.ipsec.charon
    changed_when: false
    failed_when: strongswan_result_apparmor_validation.rc != 0
    register: strongswan_result_apparmor_validation
    when: strongswan_result_apparmor_updated.changed

  - name: Reload apparmor profile for strongswan
    command: apparmor_parser -r /etc/apparmor.d/usr.lib.ipsec.charon
    when: strongswan_result_apparmor_updated.changed

  - name: Initialise vars for strongswan secret material
    set_fact:
      strongswan_result_ipsec_secrets: []
      strongswan_result_ipsec_cas: []

  - name: Register variable for strongswan secret material for host
    set_fact:
      strongswan_result_ipsec_secrets: "{{ item.value }}"
    when: item.key == ansible_hostname
    loop: "{{ strongswan_ipsec_secrets|dict2items }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: Register variable for strongswan ca material for host
    set_fact:
      strongswan_result_ipsec_cas1: "{{ item.value }}"
    when: item.key == ansible_hostname
    loop: "{{ strongswan_ipsec_cas|dict2items }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: Register variable for strongswan ca material for host
    set_fact:
      strongswan_result_ipsec_cas: "{{ [ strongswan_ipsec__var_ca_list, strongswan_result_ipsec_cas1|d([]) ]|flatten }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: Register variable for strongswan secret material file names
    set_fact:
      strongswan_result_secret_filenames: "{{ strongswan_result_ipsec_secrets|json_query(strongswan_jq_fname)|json_query(strongswan_jq_acme)|json_query(strongswan_jq_step)|selectattr('filename_root', 'defined')|selectattr('filename_root','ne','')|map(attribute='filename_root')|list }}"
      strongswan_result_secret_shared_filenames: "{{ strongswan_result_ipsec_secrets|json_query(strongswan_jq_fname)|json_query(strongswan_jq_acme)|json_query(strongswan_jq_step)|selectattr('filename_root', 'defined')|selectattr('filename_root','ne','')|selectattr('ssl_cert_shared', 'defined')|selectattr('ssl_cert_shared')|map(attribute='filename_root')|list }}"
      strongswan_result_secret_cacrt_filenames: "{{ strongswan_result_ipsec_secrets|json_query(strongswan_jq_fname)|json_query(strongswan_jq_acme)|json_query(strongswan_jq_step)|selectattr('cacrt_filename_root', 'defined')|selectattr('cacrt_filename_root','ne','')|map(attribute='cacrt_filename_root')|list }}"
      strongswan_result_secret_cacrt_filenames2: "{{ strongswan_result_ipsec_cas|json_query(strongswan_jq_fname)|json_query(strongswan_jq_acme)|json_query(strongswan_jq_step)|selectattr('cacrt_filename_root', 'defined')|selectattr('cacrt_filename_root','ne','')|map(attribute='cacrt_filename_root')|list }}"
      strongswan_result_secret_installed_cacrt_filenames2: "{{ strongswan_result_ipsec_cas|json_query(strongswan_jq_fname)|json_query(strongswan_jq_acme)|json_query(strongswan_jq_step)|selectattr('cacrt_filename_root', 'defined')|selectattr('cacrt_filename_root','ne','')|selectattr('ssl_certs_installed', 'defined')|selectattr('ssl_certs_installed')|map(attribute='cacrt_filename_root')|list }}"
    no_log: "{{ not(display_no_log|default(False)) }}"
    vars:
      strongswan_jq_fname: "[?state != 'absent']"
      strongswan_jq_acme: "[?state != 'acme_absent']"
      strongswan_jq_step: "[?state != 'step_absent']"

  # The following map regexp_replace is rather fragile for syntax...
  - name: Register keynames and crtnames variables for possible strongswan secret material file names
    set_fact:
      strongswan_result_secret_keynames: "{{ strongswan_result_secret_filenames|map('regex_replace', '^(.*)$', '\\1.key')|list }}"
      strongswan_result_secret_keynames_der: "{{ strongswan_result_secret_filenames|map('regex_replace', '^(.*)$', '\\1_key.der')|list }}"
      strongswan_result_secret_shared_keynames: "{{ strongswan_result_secret_filenames|map('regex_replace', '^(.*)$', '\\1_ssl-cert.key')|list }}"
      strongswan_result_secret_crtnames: "{{ strongswan_result_secret_filenames|map('regex_replace', '^(.*)$', '\\1.crt')|list }}"
      strongswan_result_secret_crtnames_der: "{{ strongswan_result_secret_filenames|map('regex_replace', '^(.*)$', '\\1_crt.der')|list }}"
      strongswan_result_secret_cacrtnames: "{{ (strongswan_result_secret_cacrt_filenames|union(strongswan_result_secret_cacrt_filenames2))|map('regex_replace', '^(.*)$', '\\1_ca.crt')|list }}"
      strongswan_result_secret_cacrtnames_der: "{{ (strongswan_result_secret_cacrt_filenames|union(strongswan_result_secret_cacrt_filenames2))|map('regex_replace', '^(.*)$', '\\1_cacrt.der')|list }}"
      strongswan_result_secret_cacrtnames_installed: "{{ strongswan_result_secret_installed_cacrt_filenames2|map('regex_replace', '^(.*)$', '\\1_ca.crt')|list }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  - include_role: 
      name: certbot-cert-include
    vars:
      certbot_ci_state: present
      certbot_ci_cert: '{{ cert }}'
    when: cert.state|d('present') == 'present' and cert.acme_server_key|d('')
    loop: '{{ strongswan_result_ipsec_secrets }}'
    no_log: "{{ not(display_no_log|default(False)) }}"
    loop_control:
      loop_var: cert

  #  get step certs if required
  - include_role: 
      name: step-cert-include
    vars:
      step_ci_state: present
      step_ci_cert: '{{ cert }}'
    when: cert.state|d('present') == 'present' and cert.step_provisioner_key|d('')
    loop: '{{ strongswan_result_ipsec_secrets }}'
    no_log: "{{ not(display_no_log|default(False)) }}"
    loop_control:
      loop_var: cert

  - include_tasks: "clean-strongswan-certs-keys.yml"
    loop:
      - dir: /etc/ipsec.d/private
        names: "{{ strongswan_result_secret_keynames|union(strongswan_result_secret_keynames_der|union(strongswan_result_secret_shared_keynames)) }}"
      - dir: /etc/ipsec.d/certs
        names: "{{ strongswan_result_secret_crtnames|union(strongswan_result_secret_crtnames_der) }}"
      - dir: /etc/ipsec.d/cacerts
        names: "{{ strongswan_result_secret_cacrtnames|union(strongswan_result_secret_cacrtnames_der) }}"
      - dir: /usr/local/share/ca-certificates
        names: "{{ strongswan_result_secret_cacrtnames_installed }}"
    loop_control:
      loop_var: outer_item

  # Remove certbot data if required
  - include_role: 
      name: certbot-cert-include
    vars:
      certbot_ci_state: absent
      certbot_ci_cert: '{{ cert }}'
    when: cert.state|d('present') == 'acme_absent' and cert.acme_server_key|d('')
    loop: '{{ strongswan_result_ipsec_secrets }}'
    no_log: "{{ not(display_no_log|default(False)) }}"
    loop_control:
      loop_var: cert

  # Remove step data if required
  - include_role: 
      name: step-cert-include
    vars:
      step_ci_state: absent
      step_ci_cert: '{{ cert }}'
    when: cert.state|d('present') == 'step_absent' and cert.step_provisioner_key|d('')
    loop: '{{ strongswan_result_ipsec_secrets }}'
    no_log: "{{ not(display_no_log|default(False)) }}"
    loop_control:
      loop_var: cert

  # install static certs and keys etc
  - include_tasks: strongswan-config-static.yml
    when: item.state|d('present') == 'present' and item.type in ('RSA', 'ECDSA') and item.key|d()
    loop: "{{ strongswan_result_ipsec_secrets }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  # install certbot certs
  - include_tasks: strongswan-config-certbot.yml
    when: certbot_enable|d(false) and item.state|d('present') == 'present' and item.type in ('RSA', 'ECDSA') and item.acme_server_key|d()
    loop: "{{ strongswan_result_ipsec_secrets }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  # install step-cli certs
  - include_tasks: strongswan-config-step-cli.yml
    when: (step_cli_enable|d(false) or step_ca_enable|d(false)) and item.state|d('present') == 'present' and item.type in ('RSA', 'ECDSA') and item.step_provisioner_key|d()
    loop: "{{ strongswan_result_ipsec_secrets }}"
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: Install strongswan CA certs from ipsec.secrets config
    template:
      src: etc/ipsec.d/cacerts/cacert.crt
      dest: "/etc/ipsec.d/cacerts/{{ item.cacrt_filename_root }}_ca.crt"
      owner: root
      group: root
      mode: 0644
    when: item.state|d('present') == 'present' and item.type in ('RSA', 'ECDSA') and item.cacert|d()
    loop: "{{ strongswan_result_ipsec_secrets }}"
    no_log: "{{ not(display_no_log|default(False)) }}"
    notify:
      - reload strongswan secrets

  - name: Install strongswan CA certs from CA config
    template:
      src: etc/ipsec.d/cacerts/cacert.crt
      dest: "/etc/ipsec.d/cacerts/{{ item.cacrt_filename_root }}_ca.crt"
      owner: root
      group: root
      mode: 0644
    when: item.state|d('present') == 'present' and item.cacert|d()
    loop: "{{ strongswan_result_ipsec_cas }}"
    no_log: "{{ not(display_no_log|default(False)) }}"
    notify:
      - reload strongswan secrets

  - name: Install strongswan CA certs from CA config into /etc/ssl/certs
    file:
      src: '/etc/ipsec.d/cacerts/{{ item.cacrt_filename_root }}_ca.crt'
      dest: "/usr/local/share/ca-certificates/{{ item.cacrt_filename_root }}_ca.crt"
      state: link
      follow: false
      force: true
    when: item.state|d('present') == 'present' and item.cacert|d() and item.ssl_certs_installed|d(false)
    loop: "{{ strongswan_result_ipsec_cas }}"
    no_log: "{{ not(display_no_log|default(False)) }}"
    notify:
      - refresh ca certificates

  - name: Config strongswan ipsec.secrets
    template:
      src: etc/ipsec.secrets
      dest: /etc/ipsec.secrets
      owner: root
      group: root
      mode: 0640
    no_log: "{{ not(display_no_log|default(False)) }}"
    notify:
      - reload strongswan secrets

  - name: Config strongswan ipsec.conf
    template:
      src: etc/ipsec.conf
      dest: /etc/ipsec.conf
      owner: root
      group: root
      mode: 0644
    notify:
      - reload strongswan

  tags:
    - always
...
