---
# Tasks file for kvm-update
#
- block:
  - fail:
      msg: "VM type is '{{ vm.vm_type }}' - tasks file only be executed for 'kvm' types"
    when: vm.vm_type != 'kvm'

  - fail:
      msg: "VM  '{{ vm.vm_type }}' - kvm details must exist"
    when: not vm.kvm|d()

  - fail:
      msg: "VM  '{{ vm.vm_type }}' - kvm.install_type must be one of {{ vm_manage_kvm_install_types }}"
    when: vm.kvm.install_type|d('linux-image') not in vm_manage_kvm_install_types

  - fail:
      msg: "VM  '{{ vm.vm_type }}' - kvm.cloudinit_type must be one of {{ vm_manage_kvm_cloudinit_types }}"
    when: vm.kvm.cloudinit_type|d('debian') not in vm_manage_kvm_cloudinit_types

  - name: "{{ vm.name }} - see if it exists"
    virt:
      command: list_vms
    register: vm_result_list_vms

  - name: "{{ vm.name }} - get its status"
    virt:
      name: "{{ vm.name }}"
      command: status
    when: vm.name in vm_result_list_vms.list_vms
    register: vm_result_vm_status
  
  - include_tasks: 'kvm-update-{{ vm.kvm.install_type|d("linux-image") }}.yml'

  - block:
    - block:
      - name: "{{ vm.name }} - set current number of CPUs"
        command: "virsh setvcpus {{ vm.name }} --config --live {{ vm.kvm.vcpus|d(vm_default_kvm_vcpus|d(1)) }}"
        changed_when: vm_result_setvcpus_live.rc == 0
        failed_when: false
        register: vm_result_setvcpus_live

      - name: "{{ vm.name }} - enable all vCPUs in guest"
        command: "virsh guestvcpus {{ vm.name }} --cpulist {{ range(vm.kvm.vcpus|d(vm_default_kvm_vcpus|d(1)))|join(',')}} --enable"
        failed_when: false
        when: vm_result_setvcpus_live.rc == 0

      - name: "{{ vm.name }} - set config number of CPUs - 2nd try"
        command: "virsh setvcpus {{ vm.name }} --config {{ vm.kvm.vcpus|d(vm_default_kvm_vcpus|d(1)) }}"
        changed_when: false
        when: vm_result_setvcpus_live.rc != 0

      when: vm_result_vcpucount1.stdout_lines[0]|int != vm.kvm.vcpus|d(vm_default_kvm_vcpus|d(1))
      tags:
        - always

    - name: "{{ vm.name }} - get current number of CPUs again"
      command: "virsh vcpucount {{ vm.name }} --config"
      changed_when: vm_result_vcpucount1.stdout_lines[0] != vm_result_vcpucount2.stdout_lines[0]
      register: vm_result_vcpucount2

    - name: "{{ vm.name }} - get current max memory"
      shell: "virsh domstats {{ vm.name }} --balloon | grep balloon.maximum | cut -d '=' -f 2"
      changed_when: false
      register: vm_result_memory_max1

    - name: "{{ vm.name }} - set max memory"
      command: "virsh setmaxmem {{ vm.name }} --config {{ vm.kvm.max_memory|d(vm_default_kvm_max_memory|d(2048)) }}M"
      changed_when: false

    - name: "{{ vm.name }} - get current max memory again"
      shell: "virsh domstats {{ vm.name }} --balloon | grep balloon.maximum | cut -d '=' -f 2"
      changed_when: vm_result_memory_max1.stdout_lines[0] != vm_result_memory_max2.stdout_lines[0]
      register: vm_result_memory_max2

    - name: "{{ vm.name }} - get current memory"
      shell: "virsh dumpxml {{ vm.name }} | grep currentMemory | perl -pe 's/^\\s+\\<currentMemory\\s+unit=\\S+\\>([0-9]+)\\S+$/\\1/'"
      changed_when: false
      register: vm_result_memory1

    - name: "{{ vm.name }} - set current memory"
      command: "virsh setmem {{ vm.name }} --config --live {{ vm.kvm.memory|d(vm_default_kvm_memory|d(1024)) }}M"
      failed_when: false
      changed_when: vm_result_setmem_live.rc == 0
      register: vm_result_setmem_live
      when: (vm_result_memory1.stdout_lines[0]|int)/1024 !=  vm.kvm.memory|d(vm_default_kvm_memory|d(1024))

    - name: "{{ vm.name }} - set config memory - 2nd try"
      command: "virsh setmem {{ vm.name }} --config {{ vm.kvm.memory|d(vm_default_kvm_memory|d(1024)) }}M"
      changed_when: false
      when: (vm_result_memory1.stdout_lines[0]|int)/1024 !=  vm.kvm.memory|d(vm_default_kvm_memory|d(1024)) and vm_result_setmem_live.rc != 0

    - name: "{{ vm.name }} - get current memory again"
      shell: "virsh dumpxml {{ vm.name }} | grep currentMemory | perl -pe 's/^\\s+\\<currentMemory\\s+unit=\\S+\\>([0-9]+)\\S+$/\\1/'"
      changed_when: vm_result_memory1.stdout_lines[0] != vm_result_memory2.stdout_lines[0]
      register: vm_result_memory2

    - name: "{{ vm.name }} - adjust attached networks"
      command: "/usr/local/sbin/ansible-virsh-interface-helper {{ vm.name }}"
      args:
        stdin: "{{ vm.networks|d([])|to_yaml }}"
      changed_when: vm_result_virsh_interface.rc == 0 and 'Changed' in vm_result_virsh_interface.stdout_lines
      when: vm.networks|d([]) != []
      register: vm_result_virsh_interface

    - name: "{{ vm.name }} - adjust PCI/USB pass through"
      command: "/usr/local/sbin/ansible-virsh-device-helper {{ vm.name }}"
      args:
        stdin: "{{ vm.kvm.hostdev|d([])|to_yaml }}"
      changed_when: vm_result_virsh_device.rc == 0 and 'Changed' in vm_result_virsh_device.stdout_lines
      when: vm.kvm.hostdev|d([]) != []
      register: vm_result_virsh_device

    when: vm.name in vm_result_list_vms.list_vms
    tags:
      - always

  - name: "{{ vm.name }} - set CPU affinity if requested"
    command: "/usr/local/sbin/ansible-virsh-cpu-helper {{ vm.name }}"
    args:
      stdin: "{{ vm_kvm_cpu_pin_profiles[vm.kvm.cpu_pin_profile|d('default')]|to_yaml }}"
    changed_when: vm_result_virsh_pin.rc == 0 and 'Changed' in vm_result_virsh_pin.stdout_lines
    when: vm_kvm_cpu_pin_profiles != {}
    register: vm_result_virsh_pin

  - name: "{{ vm.name }} - set autostart"
    virt:
      name: "{{ vm.name }}"
      autostart: "{{ vm.kvm.autostart|d(vm_default_kvm_autostart|d(true)) }}"

  tags:
    - always
...
