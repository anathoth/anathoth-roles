#!/usr/bin/env python3
#
# {{ ansible_managed }}
#
import yaml
import yaml.loader
import os
import os.path
import sys
import subprocess
from os.path import basename
import socket
import string

SHOREWALL='/sbin/shorewall'
SYSTEMCTL='/bin/systemctl'
SYSCTL='/sbin/sysctl'
IPSET='/usr/sbin/ipset'
ACTIONS_UPDATE=['dhcp4-change', 'dhcp6-change']
ACTIONS_DELETE=['down']
UNBOUND_CONF_FILE='/etc/unbound/unbound.conf.d/99zz-network-manager-forwarding.conf'
CONFIG="""
{{ (script.config|d({}))|to_yaml }}
"""

proc_name = basename(sys.argv[0])


def get_config():
    try:
        config = yaml.load(CONFIG, Loader=yaml.loader.SafeLoader)
    except Exception as err:
        print('{0}: Invalid yaml in CONFIG - {1}'.format(proc_name, err), file=sys.stderr)
        sys.exit(200)
    if config == None:
        print('{0}: No CONFIG - program to be called after being written out by Ansible'.format(proc_name), file=sys.stderr)
        sys.exit(201)
    return config

def run_process(args, stdin='', accept_retcode=[], check=True):
    result=None
    try:
        if not stdin:
            result = subprocess.run(args, check=check, stdout=subprocess.PIPE)
        else:
            result = subprocess.run(args, input=stdin.encode(), check=check, stdout=subprocess.PIPE)
    except subprocess.SubprocessError as err:
        if err.returncode in accept_retcode:
            return err
        print("{0}: Error calling '{1}' - '{2}'".format(proc_name, ' '.join(args), err), file=sys.stderr)
        sys.exit(202)
    return result

def test_ns_addr(ip_addr):
    try:
        socket.getaddrinfo('{0}'.format(ip_addr), 0)
    except (OSError, IOError) as err:
        print("{0}: Bad name server IP address passed to script from environment - '{2}'".format(proc_name, err))
        sys.exit(203)
    return

def test_connection_id(connection_id):
    if len(connection_id) > 255:
        return False
        #print("{0}: CONNECTION_ID way too long".format(proc_name), file=sys.stderr)
        #sys.exit(204)
    if not all(c in string.printable for c in connection_id):
        return False
        #print("{0}: CONNECTION_ID icontains unprintable characters".format(proc_name), file=sys.stderr)
        #sys.exit(205)
    return True

def reload_service(unit_name):
    unit_name = config.get('systemd_unit_name', unit_name)
    # see if unit exists
    args = [SYSTEMCTL, 'is-active', unit_name]
    result = run_process(args, check=False)
    if (result.returncode == 0):
        args = [SYSTEMCTL, 'reload', unit_name]
        result = run_process(args, check=True)
    return

def short_usage(no_exit=False):
    print("Usage: '{0} <interface> <action>'".format(proc_name), file=sys.stderr)

def full_usage():
    short_usage()
    print('', file=sys.stderr)
    print("\tNM dispatcher script, to manage unbound DNS forwarding when on laptop", file=sys.stderr)

def ipset_delete(zone):
    """
    delete a zones networks from ipset
    """
    # Do the hoki-toki
    ipset_zone = '6_' + zone.get('zone', '') if zone.get('domain', '').lower() in ['ip6', 'ipv6'] else zone.get('zone', '')
    networks = zone.get('networks', [])
    # Whirl around
    args = [IPSET, 'flush', ipset_zone ] 
    result = run_process(args, accept_retcode=[0,1,2])

def ipset_update(zone):
    """
    Update ipset with  a zones networks
    """
    # Do the hoki-toki
    ipset_zone = '6_' + zone.get('zone', '') if zone.get('domain', '').lower() in ['ip6', 'ipv6'] else zone.get('zone', '')
    networks = zone.get('networks', [])
    # Whirl around
    for net in networks:
        arg_nomatch = []
        if (net and net[0] == '!'):
            arg_nomatch = ['nomatch']
            net = net [1:]
        args = [IPSET, '-A', '-exist', ipset_zone, net] + arg_nomatch
        result = run_process(args, accept_retcode=[0,1,2])
    return


if (len(sys.argv) not in [3,]):
    full_usage()
    sys.exit(206)

config = get_config()
iface = sys.argv[1]
if (len(sys.argv) == 3):
    action = sys.argv[2]
else:
    action = ''

# Deal with no device name being given for action hostname, or action connectivity_change

if  action in ['hostname', 'connectivity-change']:
    sys.exit(0)

# Get connection_id, barf if not valid
connection_id = os.environ.get('CONNECTION_ID')
if (not connection_id or not test_connection_id(connection_id)):
    if not connection_id:
        print("{0}: CONNECTION_ID not set".format(proc_name), file=sys.stderr)
    else:
        print("{0}: CONNECTION_ID invalid".format(proc_name), file=sys.stderr)
    sys.exit(207)

# Create dhcp_domains set
dhcp_domains_list = os.environ.get('DHCP6_DHCP6_DOMAIN_SEARCH', '').split() \
        + os.environ.get('DHCP6_DOMAIN_SEARCH', '').split() \
        + os.environ.get('DHCP4_DOMAIN_SEARCH', '').split() \
        + [os.environ.get('DHCP4_DOMAIN_NAME' ''),]
dhcp_domains_list = [ d.strip() for d in dhcp_domains_list if d not in ('', None) ]
dhcp_domains_set = set(dhcp_domains_list)

# Do the actions...
actions_ipset_update = config.get('actions_ipset_update', ACTIONS_UPDATE)
actions_ipset_delete = config.get('actions_ipset_delete', ACTIONS_DELETE)
zones = config.get('zones', [])
for zone in zones:
    if (action in actions_ipset_update):
        not_conns = [ c[1:] for c in zone.get('connections', []) if (c and c[0] == '!') ]
        conns = [ c for c in zone.get('connections', []) if (c and c[0] != '!') ]
        domains_set = set(zone.get('domains', []))
        if (connection_id in not_conns):
            continue
        if (conns): 
            if (connection_id not in conns):
                continue
            if (domains_set and not dhcp_domains_set.intersection(domains_set)):
                continue
        ipset_update(zone)
    elif (action in actions_ipset_delete):
        ipset_delete(zone)

sys.exit(0)
