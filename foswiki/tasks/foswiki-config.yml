---
# Configure and install foswiki
#
- block:
  - name: About enable foswiki upgrade
    debug:
      msg: "If upgrading Foswiki, set foswiki_do_upgrade=true to upgrade"

  - name: Display foswiki_do_upgrade
    debug:
      msg: "{{ foswiki_do_upgrade|d(false) }}"

  - name: Install any required Perl packages for Foswiki extenstions
    apt:
      name: "{{ foswiki_var_extension_packages }}"
      state: "{{ apt_install_state | default('present') }}"
      update_cache: false


  - include_role:
      name: apache2-include
    vars:
      site:
        state: "{{ foswiki_enable|ternary('present', 'absent') }}"
        server_name: "{{ foswiki_server_name }}"
        server_aliases: "{{ foswiki_server_aliases }}"
        server_admin: "{{ foswiki_webmaster_email }}"
        app_document_root: "{{ foswiki_install_dir }}"
        allowed_ips: "{{ foswiki_allowed_ips }}"
        root_location: true
        docroot_directory: false
        http_only: "{{ foswiki_http_only }}"
        tls_cert: "{{ foswiki_tls_cert }}"
        tls_key: "{{ foswiki_tls_key }}"
        # Site use letsencrypt/certbot certs
        certbot_cert: "{{ foswiki_certbot_cert|d(false) }}"
        # mTLS set up
        tls_cacert: "{{ foswiki_tls_cacert }}"
        conf_templated:
          - foswiki.conf

  - name: "Create '{{ foswiki_install_dir }}' install directory"
    file:
      path: "{{ foswiki_install_dir }}"
      state: directory
      owner: www-data
      group: www-data
      mode: 0750

  - name: "Extract foswiki tarball to '{{ foswiki_install_dir }}'"
    unarchive:
      src: "{{ foswiki_install_tgz }}"
      dest: "{{ foswiki_install_dir }}"
      extra_opts:
        - --strip
        - '1'
      # Unpacking tarball again replaces files?
      creates: "{{ foswiki_install_dir }}/bin"
      owner: www-data
      group: www-data
    register: foswiki_result_unarchive

  - name: "Upgrade foswiki - extract upgrade tarball to '{{ foswiki_install_dir }}'"
    unarchive:
      src: "{{ foswiki_upgrade_tgz }}"
      dest: "{{ foswiki_install_dir }}"
      extra_opts:
        - --strip
        - '1'
      owner: www-data
      group: www-data
    when: not foswiki_result_unarchive.changed and foswiki_upgrade_tgz|d() and foswiki_do_upgrade|d(false)

  - name: Initial foswiki configuration
    become: yes
    become_user: www-data
    command:
      chdir: "{{ foswiki_install_dir }}"
      argv:
        - './tools/configure'
        - '-save'
        - '-noprompt'
        - '-set'
        - '{DefaultUrlHost}=https://{{ foswiki_server_name }}'
        - '-set'
        - '{ScriptUrlPath}=/bin'
        - '-set' 
        - '{ScriptUrlPaths}{view}='
        - '-set'
        - '{PubUrlPath}=/pub'
    when: foswiki_result_unarchive.changed

  - name: Set foswiki admin password
    become: yes
    become_user: www-data
    command:
      chdir: "{{ foswiki_install_dir }}"
      cmd: "perl -CA tools/configure -save -set {Password}='{{ foswiki_admin_password }}'"
    changed_when: false
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: Create foswiki configure directory
    file:
      path: "{{ foswiki_install_dir }}/working/configure"
      state: directory
      owner: www-data
      group: www-data
      mode: 0775

  - name: Create foswiki downloads directory
    file:
      path: "{{ foswiki_install_dir }}/working/configure/download"
      state: directory
      owner: www-data
      group: www-data
      mode: 0775

  - name: Place extension packages into download directory
    copy:
      src: "{{ item }}"
      dest: "{{ foswiki_install_dir }}/working/configure/download/{{ item.split('/')|last }}"
      owner: www-data
      group: www-data
      mode: 0664
    loop: "{{ foswiki_extension_tgzs }}"

  - include_tasks: foswiki-install-plugin.yml
    loop: "{{ foswiki_var_extensions }}"
    loop_control:
      loop_var: plugin

  - name: Place patches into download directory
    copy:
      src: "{{ item }}"
      dest: "{{ foswiki_install_dir }}/working/configure/download/{{ item.split('/')|last }}"
      owner: www-data
      group: www-data
      mode: 0664
    loop: "{{ foswiki_patches }}"

  - name: Apply any patches
    become: yes
    become_user: www-data
    shell:
      chdir: "{{foswiki_install_dir}}"
      cmd: "cat {{ foswiki_install_dir }}/working/configure/download/{{ item.split('/')|last }}|patch -p1 -sNb --dry-run && cat {{ foswiki_install_dir }}/working/configure/download/{{ item.split('/')|last }}|patch -p1 -sNb"
    changed_when: foswiki_result_patch.rc == 0
    failed_when: foswiki_result_patch.rc not in [0,1] and not foswiki_result_patch.stdout|regex_search('Reversed .or previously applied. patch detected', multiline=True)
    register: foswiki_result_patch
    loop: "{{ foswiki_patches }}"

  - name: Set Foswiki settings
    become: yes
    become_user: www-data
    command:
      cmd: "/usr/local/sbin/ansible-foswiki-settings-helper {{ foswiki_install_dir }}"
      stdin: "{{ foswiki_var_settings|d([])|to_yaml }}"
      chdir: "{{ foswiki_install_dir }}"
    changed_when: foswiki_result_set_settings.rc == 0 and 'Changed' in foswiki_result_set_settings.stdout_lines
    register: foswiki_result_set_settings

  - name: Set Foswiki LDAP settings
    become: yes
    become_user: www-data
    command:
      cmd: "/usr/local/sbin/ansible-foswiki-settings-helper {{ foswiki_install_dir }}"
      stdin: "{{ foswiki_settings_ldap|d([])|to_yaml }}"
      chdir: "{{ foswiki_install_dir }}"
    changed_when: foswiki_result_set_ldap_settings.rc == 0 and 'Changed' in foswiki_result_set_ldap_settings.stdout_lines
    register: foswiki_result_set_ldap_settings
    when: foswiki_ldap_enable|d(false)

  - name: Set Foswiki LDAP password
    become: yes
    become_user: www-data
    command:
      cmd: "/usr/local/sbin/ansible-foswiki-settings-helper {{ foswiki_install_dir }}"
      stdin: "{{ foswiki_settings_ldap_password|d([])|to_yaml }}"
      chdir: "{{ foswiki_install_dir }}"
    changed_when: foswiki_result_set_ldap_password.rc == 0 and 'Changed' in foswiki_result_set_ldap_password.stdout_lines
    register: foswiki_result_set_ldap_password
    when: foswiki_ldap_enable|d(false)
    no_log: "{{ not(display_no_log|default(False)) }}"

  - name: See if email is already enabled
    become: yes
    become_user: www-data
    shell:
      cmd: "tools/configure -getcfg {EnableEmail} | perl -e 'local $/; my $stuff = eval(<STDIN>); print $stuff->{EnableEmail};'"
      chdir: "{{ foswiki_install_dir }}"
    changed_when: false
    register: foswiki_result_setting_enable_email

  - name: Enable Email if it has not already been done
    become: yes
    become_user: www-data
    command:
      chdir: "{{ foswiki_install_dir }}"
      cmd: "tools/configure -save -wizard AutoConfigureEmail -method autoconfigure"
    when: foswiki_result_setting_enable_email.stdout_lines|first|d('') == '0'

  - name: Setup Foswiki Logon LDAP/Kerberos or not
    become: yes
    become_user: www-data
    command:
      cmd: "/usr/local/sbin/ansible-foswiki-settings-helper {{ foswiki_install_dir }}"
      stdin: "{{ foswiki_settings_user_backends|d([])|to_yaml }}"
      chdir: "{{ foswiki_install_dir }}"
    changed_when: foswiki_result_user_backends.rc == 0 and 'Changed' in foswiki_result_user_backends.stdout_lines
    register: foswiki_result_user_backends

  tags:
    - always
...
