---
# Configure dms-client
#
- block:
  - name: Install software for DMS client
    apt:
      name: bind9,bind9utils,bind9-host,dnsutils
      state: "{{ apt_install_state|d('present') }}"

  - name: Create directories
    file:
      path: "{{ item.path }}"
      state: directory
      owner: "{{ item.owner|d('root') }}"
      group: "{{ item.group|d('root') }}"
      mode:  "{{ item.mode|d('0755') }}"
    loop:
      - path: /srv/dms/rsync-config
        owner: bind
        group: bind
        mode: "0755"
      - path: /var/cache/bind/slave
        owner: bind
        group: bind
        mode: "0755"

  - name: Create rsyncd configuration
    template:
      src: etc/rsyncd.conf
      dest: /etc/rsyncd.conf
      owner: root
      group: root
      mode: '0644'
    notify:
      - dms-client restart rsyncd

  - name: Create /etc/rsyncd.secret
    template:
      src: etc/rsyncd.secret
      dest: /etc/rsyncd.secret
      owner: root
      group: root
      mode: '0600'
    notify:
      - dms-client restart rsyncd

  - name: Enable rsyncd
    lineinfile:
      path: /etc/default/rsync
      regexp: '^[# ]{0,2}RSYNC_ENABLE=.*$'
      line: 'RSYNC_ENABLE=true'
    notify:
      - dms-client restart rsyncd

  - name: Create files in /srv/dms/rsync-config
    copy:
      content: ''
      dest: "/srv/dms/rsync-config/{{ item }}"
      force: no
      owner: bind
      group: bind
      mode: "{{ '0640' if item == 'rndc-remote.key' else '0644' }}"
    notify:
      - dms-client restart bind9
    loop:
      - anathoth-acls.conf
      - bind9.conf
      - bind9-replica.conf
      - logging.conf
      - options.conf

  - name: Template remote rndc key files to /srv/dms/rsync-config
    template:
      src: "srv/dms/rsync-config/{{ item }}"
      dest: "/srv/dms/rsync-config/{{ item }}"
      force: no
      owner: bind
      group: bind
      mode: "{{ '0640' if item == 'rndc-remote.key' else '0644' }}"
    notify:
      - dms-client restart bind9
    loop:
      - controls.conf
      - rndc-remote.key

  - name: Stat rndc.key
    stat:
      path: /etc/bind/rndc.key
    register: dms_client_result_rndc_key_stat

  - name: Move rndc.key
    command: mv /etc/bind/rndc.key /etc/bind/rndc-local.key
    notify:
      - dms-client restart bind9
    when: dms_client_result_rndc_key_stat.stat.exists

  - name: Stat for apparmor named config
    stat:
      path: /etc/apparmor.d/usr.sbin.named
    register: dms_client_result_stat_apparmor_named

  - name: Check that apparmor is functioning for named
    shell: apparmor_status | grep -q /usr/sbin/named
    register: dms_client_result_apparmor_status
    when: dms_client_result_stat_apparmor_named.stat.exists
    changed_when: false
    failed_when: dms_client_result_apparmor_status.rc not in [0,1,2]

  - name: Fix Apparmor named permissions
    lineinfile:
      path: /etc/apparmor.d/usr.sbin.named
      insertafter: '^[ ]+/etc/bind/.*r,$'
      line: '  /srv/dms/rsync-config/** r,'
      owner: root
      group: root
      mode: '0644'
      validate: 'apparmor_parser -d %s'
    register: dms_client_result_apparmor_name_updated
    when: dms_client_result_apparmor_status.rc == 0

  - name: Reload apparmor profile for named
    command: apparmor_parser -r /etc/apparmor.d/usr.sbin.named
    notify:
      - dms-client restart bind9
    when: dms_client_result_apparmor_name_updated.changed

  - name: Replace named.conf files
    template: 
      src: "etc/bind/{{ item }}"
      dest: "/etc/bind/{{ item }}"
      owner: root
      group: bind
      mode: '0644'
    notify:
      - dms-client restart bind9
    loop:
      - named.conf.local
      - named.conf.options
      - rndc.conf

  tags:
    - always
...
