#!/usr/bin/env python3
#
# {{ ansible_managed }}
#
import yaml
import yaml.loader
import os
import os.path
import sys
import subprocess
from os.path import basename
import socket
import string

SYSTEMCTL='/bin/systemctl'
UNBOUND_CONF_FILE='/etc/unbound/unbound.conf.d/99zz-network-manager-forwarding.conf'
CONFIG="""{{ (script.config|d({}))|to_yaml }}"""

proc_name = basename(sys.argv[0])


def get_config():
    try:
        config = yaml.load(CONFIG, Loader=yaml.loader.SafeLoader)
    except Exception as err:
        print('{0}: Invalid yaml in CONFIG - {1}'.format(proc_name, err), file=sys.stderr)
        sys.exit(200)
    if config == None:
        print('{0}: No CONFIG - program to be called after being written out by Ansible'.format(proc_name), file=sys.stderr)
        sys.exit(201)
    return config

def run_process(args, stdin='', accept_retcode=[], check=True):
    result=None
    try:
        if not stdin:
            result = subprocess.run(args, check=check, stdout=subprocess.PIPE)
        else:
            result = subprocess.run(args, input=stdin.encode(), check=check, stdout=subprocess.PIPE)
    except subprocess.SubprocessError as err:
        if err.returncode in accept_retcode:
            return err
        print("{0}: Error calling '{1}' - '{2}'".format(proc_name, ' '.join(args), err), file=sys.stderr)
        sys.exit(202)
    return result

def test_ns_addr(ip_addr):
    try:
        socket.getaddrinfo('{0}'.format(ip_addr), 0)
    except (OSError, IOError) as err:
        print("{0}: Bad name server IP address passed to script from environment - '{2}'".format(proc_name, err))
        sys.exit(203)
    return

def test_connection_id(connection_id):
    if len(connection_id) > 255:
        return False
        #print("{0}: CONNECTION_ID way too long".format(proc_name), file=sys.stderr)
        #sys.exit(204)
    if not all(c in string.printable for c in connection_id):
        return False
        #print("{0}: CONNECTION_ID icontains unprintable characters".format(proc_name), file=sys.stderr)
        #sys.exit(205)
    return True

def reload_service(unit_name):
    unit_name = config.get('systemd_unit_name', unit_name)
    # see if unit exists
    args = [SYSTEMCTL, 'is-active', unit_name]
    result = run_process(args, check=False)
    if (result.returncode == 0):
        args = [SYSTEMCTL, 'reload', unit_name]
        result = run_process(args, check=True)
    return

def short_usage(no_exit=False):
    print("Usage: '{0} <interface> <action>'".format(proc_name), file=sys.stderr)

def full_usage():
    short_usage()
    print('', file=sys.stderr)
    print("\tNM dispatcher script, to manage unbound DNS forwarding when on laptop", file=sys.stderr)

if (len(sys.argv) not in [3,]):
    full_usage()
    sys.exit(206)

config = get_config()
iface = sys.argv[1]
if (len(sys.argv) == 3):
    action = sys.argv[2]
else:
    action = ''

# Deal with no device name being given for action hostname, or action connectivity_change

if  action in ['hostname', 'connectivity-change']:
    sys.exit(0)

# Do the actions...
actions_write_file = config.get('actions_write_file')
actions_rm_file = config.get('actions_rm_file')
if (action in actions_write_file):
    contents = """#
# Forwarding data from Network Manager
# Auto generated from NM connection '{connection_id}'
#
forward-zone:
  name: "."
"""
    connection_id = os.environ.get('CONNECTION_ID', '----')
    if not test_connection_id(connection_id):
        connection_id = '----'
    contents = contents.format(connection_id=connection_id)
    forward_no_cache = config.get('forward_no_cache', None)
    if forward_no_cache is not None:
        forward_no_cache_str = "  forward-no-cache: {0}\n".format('yes' if forward_no_cache else 'no')
        contents += forward_no_cache_str
    ip6_nameservers = os.environ.get('IP6_NAMESERVERS', '').split()
    for ns in ip6_nameservers:
        test_ns_addr(ns)
        contents += "  forward-addr: {0}\n".format(ns)
    ip4_nameservers = os.environ.get('IP4_NAMESERVERS', '').split()
    for ns in ip4_nameservers:
        test_ns_addr(ns)
        contents += "  forward-addr: {0}\n".format(ns)
    # Write out file
    try:
        out = open(UNBOUND_CONF_FILE,'w')
        print(contents, file=out)
        out.close()
    except (OSError, IOError) as err:
        print("{0}: Error writing unbound conf file '{1}' - '{2}'".format(proc_name, UNBOUND_CONF_FILE, err), file=sys.stderr)
        sys.exit(207)
    reload_service('unbound.service')
elif (action in actions_rm_file):
    try:
        os.remove(UNBOUND_CONF_FILE)
    except FileNotFoundError:
        pass
    except (OSError,IOError) as err:
        print("{0}: Error removing unbound conf file '{1}' - '{2}'".format(proc_name, UNBOUND_CONF_FILE, err), file=sys.stderr)
        sys.exit(208)
    reload_service('unbound.service')
    
sys.exit(0)
