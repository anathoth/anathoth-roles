#
#  {{ ansible_managed }}
#
#
# This file is sourced by the mtx-changer script every time it runs.
#   You can put your site customization here, and when you do an
#   upgrade, the process should not modify this file.  Thus you
#   preserve your mtx-changer configuration.
#

# Set to 1 if you want to do offline before unload
offline={{ bareos_sd_mtx_changer.offline|d('0') }}

# Set to amount of time in seconds to wait after an offline
offline_sleep={{ bareos_sd_mtx_changer.offline_sleep|d('0') }}

# Set to amount of time in seconds to wait after a load
load_sleep={{ bareos_sd_mtx_changer.load_sleep|d('0') }}

# Set to 1 to do an inventory before a status. Not normally needed.
inventory={{ bareos_sd_mtx_changer.inventory|d('0') }}

# If you have a VXA PacketLoader, it might display a different
#  Storage Element line, so try setting the following to 1
vxa_packetloader={{ bareos_sd_mtx_changer.vxa_packetloader|d('0') }}

# Set to 1 if you want debug info written to a log
debug_log={{ bareos_sd_mtx_changer.debug_log|d('0') }}


# mt status output
# SunOS     No Additional Sense
# FreeBSD   Current Driver State: at rest.
# Linux     ONLINE
#  Note Debian has a different mt than the standard Linux version.
#    When no tape is in the drive it waits 2 minutes.
#    When a tape is in the drive, it prints user unfriendly output.
#  Note, with Ubuntu Gusty (8.04), there are two versions of mt,
#    so we attempt to figure out which one.
#

OS=`uname`
case ${OS} in
  SunOS)
    ready="No Additional Sense"
    ;;
  FreeBSD)
    ready="Current Driver State: at rest."
    ;;
  Linux)
    ready="ONLINE"
    if test -f /etc/debian_version ; then
       mt --version|grep "mt-st" >/dev/null 2>&1
       if test $? -eq 1 ; then
          ready="drive status"
       fi
    fi
  ;;
esac

