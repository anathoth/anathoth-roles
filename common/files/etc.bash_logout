# ~/.bash_logout: executed by bash(1) when login shell exits.

# when leaving the console clear the screen to increase privacy

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi

# Set title bar to something sensible.
case $TERM in
        *xterm*)
                echo -e "\033]0;xterm\007"
        ;;
esac

# when leaving the console clear the screen to increase privacy

case "`tty`" in
    /dev/tty[0-9]) clear
esac

