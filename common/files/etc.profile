# /etc/profile: system-wide .profile file for the Bourne shell (sh(1))
# and Bourne compatible shells (bash(1), ksh(1), ash(1), ...).

if [ -x  /etc/environment.sh ]; then
  . /etc/environment.sh
fi
  
# Source /etc/profile.d
if [ -d /etc/profile.d ]; then
  for F in /etc/profile.d/*.sh; do
    if [ -r "$F" ]; then
      . "$F"
    fi
    unset F
  done
fi

#PATH="/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/usr/games"

#export PATH 

