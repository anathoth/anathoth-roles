#!/bin/bash

if [ -z "$LANG" -o "$LANG" = "C" ]; then
	if [ -f /etc/environment ]; then
		. /etc/environment
		export LANG
	fi
fi

# Set up fancy prompt
#PROMPT_CORE="\h: -\u- [\w]"
PROMPT_CORE="\h: -${USER}- [\w]"
PROMPT="\n${PROMPT_CORE} \n\\$ "
#PROMPT_CORE="\u@\h:\w\\$"
#PROMPT="\n${PROMPT_CORE} "
case $TERM in
    *xterm*)
        export PS1="\[\033]0;${PROMPT_CORE}\007\]${PROMPT}"
        #export TERM=xterm
        #stty erase ^H  # Fix Backspace key
        ;;
            linux)
        if [ -n "$CONSOLE" ] ; then
            export TERM=vt100; tset
        fi
        export PS1="$PROMPT"
        ;;
    *)
        export PS1="$PROMPT"
        ;;
esac
unset PROMPT PROMPT_CORE

# Setup PATH
{% if binsh_extra_path|d() %}
PATH="$HOME/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/sbin:/usr/sbin:/usr/local/sbin:/usr/games:{{ binsh_extra_path|join(':') }}"
{% else %}
PATH="$HOME/bin:/usr/local/bin:/usr/bin:/bin:/usr/bin/X11:/sbin:/usr/sbin:/usr/local/sbin:/usr/games"
{%endif %}
export PATH

# Setup less
export LESS=-MMRi

# Stop vtysh from being a pain in the arse...
# export VTYSH_PAGER="/bin/more"

# Set up JAVA_HOME for preferred default Java installation
# export JAVA_HOME="/usr/lib/jvm/java-1.5.0-sun"

{% if http_proxy_use_envvars|default(false) and not squid_enable|default(false) %}
# HTTP proxy shell vars
export http_proxy={{ http_proxy_http }}
export https_proxy={{ http_proxy_https|default(http_proxy_http) }}
{% if http_proxy_no_proxy_list is defined %}
export no_proxy={{ http_proxy_no_proxy_list|ipwrap|join(',') }}
{% endif%}
{% endif %}

{{ (binsh__var_envvars|default([]))|flatten|join('\n') }}

# Set up the umask for file creation on the system.  This helps Samba
if [ $UID -ge 1000 ]; then
        umask 002
else
	umask 022
fi

alias lo=exit
alias sstrip='strip --remove-section=.note --remove-section=.comment -s'

psg () {
	ps axw | grep $1 | grep -v grep
}

