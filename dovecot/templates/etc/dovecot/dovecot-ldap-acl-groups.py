#!/usr/bin/env python3
#
#  {{ ansible_managed }}
#
# A Dovecot post-login script for IMAP. This creates environment
# ACL_GROUPS with a comma-separated list of the user's LDAP group
# memberships and then execs the Dovecot IMAP handler.
#

import ldap, os, sys;

ldap_hosts = "{{ dovecot_ldap_hosts }}"
ldap_urls = ['ldap://{0}'.format(x) for x in ldap_hosts.split()]
ldapUrls = ' '.join(ldap_urls)
bindAccount = "{{ dovecot_ldap_binddn }}"
bindPw = "{{ dovecot_ldap_bindpw }}"
tls_enable = {{ dovecot_tls_enable|ternary('True', 'False') }}
tls_require_cert = "{{ dovecot_tls_require_cert }}"
debug = False
#hosts = smb-ad1.smbzzz.anathoth.net smb-ad2.smbzzz.anathoth.net
#dn = cn=dovecot,cn=Managed Service Accounts,dc=smbzzz,dc=anathoth,dc=net
#dnpass = BEEP

searchBase = "{{ dovecot_ldap_search_base }}"
scope = "{{ dovecot_ldap_scope }}"
#searchFilter = "(&(objectClass=person)(sAMAccountName={0}))"
searchFilter = '{{ dovecot_ldap_search_filter }}'
searchFilter = searchFilter.replace('%u', '{0}')

tls_req_cert_lookup = {'never': ldap.OPT_X_TLS_NEVER,
        'hard': ldap.OPT_X_TLS_HARD,
        'demand': ldap.OPT_X_TLS_DEMAND,
        'allow': ldap.OPT_X_TLS_ALLOW,
        'try': ldap.OPT_X_TLS_TRY
        }
scope_lookup = {'base': ldap.SCOPE_BASE,
        'onelevel': ldap.SCOPE_ONELEVEL,
        'subtree': ldap.SCOPE_SUBTREE
        }

user = os.environ["USER"]
groups = []

ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
l = ldap.initialize(ldapUrls)
l.set_option(ldap.OPT_REFERRALS, 0)
l.set_option(ldap.OPT_PROTOCOL_VERSION, 3)
if debug:
    l.set_option(ldap.OPT_DEBUG_LEVEL, 255)
if tls_enable:
    l.set_option(ldap.OPT_X_TLS_CACERTFILE, "{{ dovecot_tls_cafile }}" )
    l.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, tls_req_cert_lookup[tls_require_cert.lower()])
    l.set_option(ldap.OPT_X_TLS_NEWCTX, 0)
    l.start_tls_s()
l.bind(bindAccount, bindPw, ldap.AUTH_SIMPLE)
res = l.search_s(searchBase, scope_lookup[scope.lower()],
                 searchFilter.format(os.environ["USER"]),
                 ['memberOf'])
for dn, entry in res:
    if not dn:
        continue
    try:
        for g in entry['memberOf']:
            # Returns 'cn=All UK staff,ou=Groups,dc=example,dc=com' etc.
            # Fish out 'All UK staff' as group name.
            groups.append(g.decode().split(',', 1)[0][3:])
    except KeyError:
        pass    # User in no groups.
        
os.environ["ACL_GROUPS"] = ",".join(groups)
try:
    os.environ["USERDB_KEYS"] += " acl_groups"
except KeyError:
    os.environ["USERDB_KEYS"] = "acl_groups"

# print(os.environ["ACL_GROUPS"])

l.unbind_s()

os.execv(sys.argv[1], sys.argv[1:])
sys.exit(1) # In case above fails
