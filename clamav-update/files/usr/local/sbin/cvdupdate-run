#!/usr/bin/env python3

import sys
import subprocess
import re
import yaml
import yaml.loader
from os.path import basename
import getopt

proc_name = basename(sys.argv[0])
debug = False
dry_run = False
changed = False

CFG_FILE = '/etc/clamav/cvdupdate-run.conf'
CVDUPDATE = '/usr/bin/cvdupdate'
CVDUPDATE_ARGS = ['update', '-V']
CVDUPDATE_SUBJECT = 'CVDUpdate report'
CVDUPDATE_EMAIL = 'clamav'

def run_cmd(args, stdin='', shell=False):
    global dry_run
    global debug
    result = None
    args = [str(x) for x in args] if type(args) == list else args
    debug_cmd = ' '.join(args) if type(args) == list else args
    if debug or dry_run:
        print("Executing: '{0}'".format(debug_cmd), file=sys.stderr)
    if dry_run:
        return result
    try:
        if not stdin:
            result = subprocess.run(args, shell=shell, check=True, stdout=subprocess.PIPE)
        else:
            result = subprocess.run(args, input=stdin.encode(), shell=shell, check=True, stdout=subprocess.PIPE)
    except subprocess.SubprocessError as err:
        print("{0}: Error calling '{1}' - '{2}'"
              .format(proc_name, debug_cmd, err),
              file=sys.stderr)
        sys.exit(2)
    return result

def run_cmd_changed(args, stdin='', shell=False):
    global dry_run
    result = run_cmd(args, stdin=stdin, shell=shell)
    if dry_run and result == None:
        return False
    try:
        run_cmd_changed_output = result.stdout.decode().strip()
    except Exception as err:
        print("{0}: Error decoding run_cmd_changed_output - '{1}'"
              .format(proc_name, err),
              file=sys.stderr)
        sys.exit(2)
    if not run_cmd_changed_output:
       return False
    run_cmd_changed_output = run_cmd_changed_output.strip().split('\n')
    changed = 'Changed' in run_cmd_changed_output
    return changed

def run_cmd_show(args, ret_codes=[]):
    global dry_run
    global debug
    args = [str(x) for x in args] if type(args) == list else args
    debug_cmd = ' '.join(args) if type(args) == list else args
    if debug or dry_run:
        print("Executing: '{0}'".format(debug_cmd), file=sys.stderr)
    if dry_run:
        return ([], None)
    try:
        result = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    except subprocess.SubprocessError as err:
        print("{0}: Error calling '{1}' - '{2}'"
              .format(proc_name, ' '.join(args), err),
              file=sys.stderr)
        sys.exit(2)
    if (ret_codes and result.returncode not in ret_codes):
        try:
            err = result.stdout.decode().strip()
        except Exception as err:
            print("{0}: Error decoding stdout - '{1}'"
                  .format(proc_name, err),
                  file=sys.stderr)
            sys.exit(3)
        print("{0}: Error calling '{1}' - '{2}'"
              .format(proc_name, ' '.join(args), err),
              file=sys.stderr)
        sys.exit(2)
    try:
        run_cmd_show_output = result.stdout.decode().strip()
    except Exception as err:
        print("{0}: Error decoding run_cmd_show_output - '{1}'"
              .format(proc_name, err),
              file=sys.stderr)
        sys.exit(2)
    if not run_cmd_show_output:
       return ([], result)
    run_cmd_show_output = run_cmd_show_output.strip().split('\n')
    return (run_cmd_show_output, result)

def flatten(itr):
    if type(itr) not in (list,):
        yield itr
    else:
        for x in itr:
            try:
                yield from flatten(x)
            except TypeError:
                yield x

def read_yaml():
    """Read domain config from CFG_FILE as yaml"""
    try:
        with open(CFG_FILE, 'r') as f:
            cfg = yaml.load(f,  Loader=yaml.loader.SafeLoader)
    except Exception as err:
        print("{0}: Invalid yaml in '{1}' - {2}".format(proc_name, CFG_FILE, err), file=sys.stderr)
        sys.exit(2)

    if cfg == None:
        print("{0}: No Input - please fill out '{1}'".format(proc_name, CFG_FILE), file=sys.stderr)
        sys.exit(2)

    return cfg

def short_usage():
    print("Usage: '{0} [-dhnN]'".format(proc_name), file=sys.stderr)

def full_usage():
    short_usage()
    print('', file=sys.stderr)
    print("\tFor running clamdscan mail scanning", file=sys.stderr)
    print('', file=sys.stderr)
    print("\t-d --debug        Print out debugging", file=sys.stderr)
    print("\t-h --help         Print this help message", file=sys.stderr)
    print("\t-n --dry-run      Dry run", file=sys.stderr)
    print("\t-N --no-email     No email, print results", file=sys.stderr)

def main():
    global debug
    global dry_run
    global changed
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], 'dhnN', ['debug',  'help', 'dry-run', 'no-email',])
    except getopt.GetoptError:
        short_usage()
        sys.exit(2)

    confirm = False
    no_email = False
    dry_run = False
    debug = False
    for opt in opts:
        if opt[0] in ('-d', '--debug'):
            debug = True
        if opt[0] in ('-n', '--dry-run'):
            dry_run = True
        if opt[0] in ('-N', '--no-email'):
            no_email = True
        if opt[0] in ('-h', '--help'):
            full_usage()
            sys.exit(0)

    if (len(args) != 0):
        short_usage()
        sys.exit(2)

    cfg = read_yaml()
    download_failed = False
    output = []
    exit_codes = [0,1,2]
    email_addrs = cfg.get('email_addresses', [CVDUPDATE_EMAIL,])
    subject = cfg.get('email_subject', CVDUPDATE_SUBJECT)
    confirm = cfg.get('always_email', False)
    args = [CVDUPDATE, ] + CVDUPDATE_ARGS
    output, result = run_cmd_show(args, ret_codes=exit_codes)
    if (not no_email and not confirm and result.returncode == 0):
        sys.exit(0)
    output = '\n'.join(output)
    subject = subject.upper() + ' - UPDATE FAILED'
    if no_email:
        print(output, file=sys.stdout)
        sys.exit(0)
    args = ['/usr/bin/mail', '-s', subject,] + email_addrs
    run_cmd(args, stdin=output)

if __name__ == "__main__":
    main()



