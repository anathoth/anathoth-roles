---
# Split out here to reduce skipped messages
- block:
  - name: keycloak-include - work out file name
    set_fact:
      keycloak_include_result_acf_file: "{{ keycloak_include_var_site.client_name }}_{{ keycloak_include_var_site.config_format }}.{{ 'zip' if keycloak_include_var_site.config_format == 'mod-auth-mellon' else 'xml' }}"

  - name: keycloak-include - create file dir  on ansible host
    delegate_to: localhost
    become: no
    file:
      path: files/keycloak-data
      state: directory
      mode: '0700'
    register: keycloak_include_result_ctrl_dir

  - name: keycloak-include - find older files
    delegate_to: localhost
    become: no
    find:
      paths: files/keycloak-data
      age: "{{ keycloak_include_acf_keep_age }}"
      recurse: yes
    register: keycloak_include_result_old_data

  - name: keycloak-include - remove older files
    file:
      path: "{ item.path }}"
      state: absent
    loop: "{{ keycloak_include_result_old_data.files }}"


  - name: keycloak-include - stat ACF file on controller
    delegate_to: localhost
    become: no
    stat:
      path: "files/keycloak-data/{{ keycloak_include_result_acf_file }}"
    register: keycloak_include_result_ctrl_stat

  - name: keycloak-include - force ACF update
    debug:
      msg: "Set keycloak_force_acf_update=true to immediately refresh ACF data"

  - name: keycloak-include - Display keycloak_force_acf_update
    debug:
      msg: "{{ keycloak_force_acf_update|d(false) }}"

  - block:
    - name: keycloak-include - create tmpdir on server
      delegate_to: '{{ server }}'
      tempfile:
        state: directory
        prefix: keycloak-include_
      register: keycloak_include_result_remote_tmpdir

    - name: keycloak-include - get keycloak config on server
      delegate_to: '{{ server }}'
      expect: 
        command: "/usr/local/sbin/keycloak-get-adapter-config {{ keycloak_include_var_site.realm }} {{ keycloak_include_var_site.client_name }} {{ keycloak_include_var_site.config_format }}"
        chdir: "{{ keycloak_include_result_remote_tmpdir.path }}"
        creates: "{{ keycloak_include_result_remote_tmpdir.path }}/{{ keycloak_include_result_acf_file }}"
        echo: yes
        responses:
          'Enter password: ': "{{ keycloak_admin_password }}"
      no_log: "{{ not(display_no_log|default(False)) }}"
      failed_when: keycloak_include_result_get_config.rc not in [0,1]
      register: keycloak_include_result_get_config

    - name: keycloak-include - copy file to controller
      delegate_to: '{{ server }}'
      fetch:
        flat: true
        src: "{{ keycloak_include_result_remote_tmpdir.path }}/{{ keycloak_include_result_acf_file }}"
        dest: "files/keycloak-data/{{ keycloak_include_result_acf_file }}"

    - name: keycloak-include - remove file on server
      delegate_to: '{{ server }}'
      command: "shred -u {{ keycloak_include_result_remote_tmpdir.path }}/{{ keycloak_include_result_acf_file }}"

    - name: keycloak-include - remove tmpdir on server
      delegate_to: '{{ server }}'
      file:
        path: '{{ keycloak_include_result_remote_tmpdir.path }}'
        state: absent

    when: keycloak_force_acf_update|d(false) or not keycloak_include_result_ctrl_stat.stat.exists|d(false)
    tags:
      - always

  - block:
    - name: keycloak-include - create tmpdir on target
      tempfile:
        state: directory
        prefix: keycloak-include_
      register: keycloak_include_result_target_tmpdir
      changed_when: false

    - name: keycloak-include - unarchive mod-auth-mellon ACF data to target
      unarchive:
        src: "keycloak-data/{{ keycloak_include_result_acf_file }}"
        dest: "{{ keycloak_include_result_target_tmpdir.path }}"
      changed_when: false

    - name: keycloak-include - copy ACF data files into place
      copy:
        src: '{{ keycloak_include_result_target_tmpdir.path }}/{{ keycloak_include_var_site.client_name }}/{{ item }}'
        dest: "{{ keycloak_include_var_site.acf_dest }}/{{ item }}"
        owner: "{{ keycloak_include_var_site.acf_owner }}"
        group: "{{ keycloak_include_var_site.acf_group }}"
        mode: "{{ keycloak_include_var_site.acf_mode|d('0600', true) }}"
        remote_src: true
      loop:
        - idp-metadata.xml
        - sp-metadata.xml

    - name: keycloak-include - find data files
      find:
        paths: "{{ keycloak_include_result_target_tmpdir.path }}"
        recurse: yes
      register: keycloak_include_result_mellon_files
      changed_when: false

    - name: keycloak-include - shred data files
      command: 'shred -u {{ item.path }}'
      loop: '{{ keycloak_include_result_mellon_files.files }}'
      changed_when: false

    - name: keycloak-include - remove tmpdir on target
      file:
        path: '{{ keycloak_include_result_target_tmpdir.path }}'
        state: absent
      changed_when: false

    when: keycloak_include_var_site.config_format == 'mod-auth-mellon'
    tags:
      - always

  - block:
    - name: keycloak-include - copy ACF data to target
      copy:
        src: "keycloak-data/{{ keycloak_include_result_acf_file }}"
        dest: "{{ keycloak_include_var_site.acf_dest }}"
        owner: "{{ keycloak_include_var_site.acf_owner }}"
        group: "{{ keycloak_include_var_site.acf_group }}"
        mode: "{{ keycloak_include_var_site.acf_mode|d('0600', true) }}"

    when: keycloak_include_var_site.config_format != 'mod-auth-mellon'
    tags:
      - always


  tags:
    - always
...
